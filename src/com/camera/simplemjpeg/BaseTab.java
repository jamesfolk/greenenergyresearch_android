package com.camera.simplemjpeg;

import android.os.Bundle;

public interface BaseTab
{
	public void setBundle(Bundle bundle);
}