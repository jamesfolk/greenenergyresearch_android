package com.camera.simplemjpeg.PhidgetData;

public interface AbstractPhidgetData 
{
	public abstract int getNumYGraphValues();
	public abstract double getYGraphValue(int index);
}
